const { expect } = require("chai");
const { ethers } = require('hardhat');
const hre = require("hardhat");
import { BigNumber, Contract, Signer } from "ethers";

describe("Bancor", () => {

    let accounts: Signer[];

    beforeEach(async () => {
        accounts = await ethers.getSigners();

    });

    describe("Bancor", () => {
        it('Check Vitaliks balance on Mainnet', async () => {

            await hre.network.provider.request({
                method: "hardhat_impersonateAccount",
                params: ["0xab5801a7d398351b8be11c439e05c5b3259aec9b"]
            })

            const signer = await ethers.provider.getSigner("0xab5801a7d398351b8be11c439e05c5b3259aec9b")

            let bal = await signer.getBalance()
            let block = await ethers.provider.getBlock()

            console.log("Vitalik's balance at block ", block.number)
            console.log(ethers.utils.formatUnits(bal, "ether"), "ETH")
            console.log("\n\nINITIATE RUG\n\n")

            const tx = await signer.sendTransaction({
                to: ethers.constants.AddressZero,
                value: ethers.constants.WeiPerEther,
            })

            bal = await signer.getBalance()
            block = await ethers.provider.getBlock()

            console.log("Vitalik's balance at block ", block.number)
            console.log(ethers.utils.formatUnits(bal, "ether"), "ETH")
        });
    });
});